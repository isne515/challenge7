#include <iostream>
#include "strvar.h" //include stringvar header

using namespace std;
using namespace book;

void conversation(int max_size); // function that stored conversation

int main()
{
	conversation(100); //Declare Max size of charactor is 100
	return 0;
}

void conversation(int max_size)
{
	StringVar word1(max_size), word2(max_size); //for store word
	int choose, next;
	char character;

	cout << "Input your first-word : "; //input word one
	word1.input_line(cin);

	cout << "Input your second-word : "; //input word two
	word2.input_line(cin);

	cout << endl;
	if (word1 == word2) //Compare 2 words
		cout << "[Same]" << endl; //If same show this
	else
		cout << "[Not same]" << endl; //If not same show this

	StringVar word = word1 + word2; //linked word together
	cout << endl;
	cout << "The conversation is :  " << word << endl; //Show conversation

	cout << endl;
	cout << "Input the number of character you want to choose (Ex. first char is 1 second is 2) : ";
	//user choose number of character
	cin >> choose;

	cout << endl;
	cout << "Your character is : " << word.one_char(choose) << endl; //Show character that user choose

	cout << endl;
	cout << "Input the number of character you need to choose and change\n";
	cout << "(Ex. choose position 1st next is new character) : "; //user choose number of character and change
	cin >> choose >> character;
	word.set_char(choose, character);

	cout << endl;
	cout << "Your character is : " << word << endl; //Show conversastion after change a character

	cout << endl;
	cout << "Input the number of character you need to choose and number of next character\n";
	cout << "(Ex. word is 'Hello' if input '2 2' output is 'ell') : ";
	//user choose number of character and number of next charactor
	cin >> choose >> next;

	cout << "Your character is : ";
	word.copy_piece(choose, next); //Show the character from choose to next character
	cout << endl;

	cout << "Input word for using >> : "; //input word for using operator >>
	cin >> word;

	cout << endl;
	cout << "Your word is: " << word; //Show the word
	cout << endl;

	system("pause");
}