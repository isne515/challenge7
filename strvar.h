﻿#ifndef STRVAR_H
#define STRVAR_H
#include <iostream>
using namespace std;
namespace book
{
	class StringVar
	{
	public:
		StringVar(int size);
		//Constructor when size of string is specified.
		StringVar();
		//Constructor with default size of 100.
		StringVar(const char a[]);
		//Precondition: The array a contains characters and â€˜\0â€².
		//Constructor using array as values.
		StringVar(const StringVar& string_object);
		//Copy Constructor.
		~StringVar();
		//Destructor, returns memory to heap.
		int length() const;
		//Returns the length of the current string.
		void input_line(istream& ins);
		//Precondition: if ins is a file input stream attached to a file.
		//The next text up to â€˜\nâ€™, or the capacity of the stringvar is copied.
		friend ostream& operator <<(ostream& outs, const StringVar& the_string);
		//Precondition: if outs is a file it has been attached to a file.
		//Overloads the << operator to allow output to screen

		void copy_piece(int select, int nextchar); //show character from first number that use select to last number that user select
		void set_char(int select, char input); //pick the character that user select and change to new charater that user input
		char one_char(int select); //pick the character that user select

		friend StringVar operator+(StringVar &str1, StringVar &str2); //Operator + for linked word one and word two
		friend bool operator==(StringVar &str1, StringVar &str2); //Compare between two words
		friend istream& operator >> (istream& outs, const StringVar& str); //Function that make program can use operator >> for store word to variable

	private:
		char *value; 
		int max_length; //declared maximum length of the string.
	};
}
#endif
